import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { Router } from "@angular/router";

import { Page } from 'tns-core-modules/ui/page';
import { Color } from 'tns-core-modules/color';
import { View } from 'tns-core-modules/ui/core/view';
import { TextField } from 'tns-core-modules/ui/text-field';

import { User } from "../../shared/user/user";
import { UserService } from "../../shared/user/user.service";
import { setHintColor } from "../../utils/hint-util";

@Component({
    selector: "gr-main",
    providers: [UserService],
    templateUrl: 'app/pages/login/login.component.html',
    styleUrls: ['app/pages/login/login-common.css', 'app/pages/login/login.css']
})
export class LoginComponent implements OnInit{
    public user: User;
    public isLoggingIn = true;
    public isLoading = false;

    @ViewChild('container') container: ElementRef;
    @ViewChild('email') email: ElementRef;
    @ViewChild('password') password: ElementRef;

    constructor(private router: Router, private userService: UserService, private page: Page) {
        this.user = new User();
        this.user.email = 'admin@test.com';
        this.user.password = 'password';
    }

    public ngOnInit() {
        this.page.actionBarHidden = true;
        this.page.backgroundImage = 'res://bg_login';
        this.isLoading = false;
    }

    public submit() {
        if (!this.user.isValidEmail()) {
            alert('Enter a valid email address');
            return;
        }
        if (this.isLoggingIn) {
            this.login();
        } else {
            this.signUp();
        }
    }

    public toggleDisplay() {
        this.isLoggingIn = !this.isLoggingIn;
        this.setTextFieldColors();
        const container = <View>this.container.nativeElement;
        container.animate({
            backgroundColor: this.isLoggingIn ? new Color('white') : new Color('#301217'),
            duration: 200
        });
    }

    private login() {
        this.isLoading = true;
        this.userService.login(this.user).subscribe(
            () => {
                this.isLoading = false;
                this.router.navigate(['/list'])
            },
            (error) =>  {
                this.isLoading = false;
                alert('Unfortunately we could not find your account.');
            }
        );
    }

    private signUp() {
        this.isLoading = true;
        this.userService.register(this.user).subscribe(
            () => {
                this.isLoading = false;
                alert('Your account was successfully created.');
                this.toggleDisplay();
            },
            () => {
                this.isLoading = false;
                alert("Unfortunately we were unable to create your account.")
            }
        );
    }

    private setTextFieldColors() {
        const emailTextField = <TextField>this.email.nativeElement;
        const passwordTextField = <TextField>this.password.nativeElement;

        const mainTextColor = new Color(this.isLoggingIn ? 'black' : '#AA81C4');
        emailTextField.color = mainTextColor;
        passwordTextField.color = mainTextColor;

        const hintColor = new Color(this.isLoggingIn ? '#ACA6A7' : '#AA81C4');
        setHintColor({ view: emailTextField, color: hintColor });
        setHintColor({ view: passwordTextField, color: hintColor });
    }
}
